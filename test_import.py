import time
import django_processes as dp


@dp.monitored
def echo():
    time.sleep(1)

@dp.monitored
def echo2():
    time.sleep(2)

def worker(*args):
    echo()
    echo2()
    return 'done'
